# ULEAM FORM APP

Uleam form app es una aplicacion destina a la creacion de encuesta.
Para el desarrollo de la aplicacion se usaron conocimientos en clase y agregrados en el proyecto como son PWA (Progesive Web App), SPA (Single Page Aplicacion), conexion a una base de datos, etc.

_Puede revisar el producto final en el siguiente [enlace](https://uleam-form.herokuapp.com/)_

### Instalación 🔧

_Para instalar la aplicacion hay que ejecutar el siguiente comando de node.js_

```
npm install
```

_En cada una de las carpetas y escribir el ultimo comando de node.js_

```
npm start
```

_en cada carpeta y usar el localhost:3000 para visualizar la aplicacion hecha en React_

## Despliegue 📦

_Para el despliegue de la aplicacion usar el siguiente repositorio [clic](https://gitlab.com/LeonardoT-V/web-heroku) que tiene una estructura diferente para que heroku puede leer el packaje.json para inicar con la instalacion de librerias e inicio de aplicacion. Ademas de contar con la cadena de conexion de postgresql de heroku_

**_La aplicacion fue publicada en heroku_**

## Construido con 🛠️

_La aplicacion cuenta con las siguentes librerias:_

* [Express](https://expressjs.com/es/) - usado para el servidor y creacion de la api
* [pg](https://node-postgres.com/) - Usado para la conexion con postgresql
* [Cors](https://www.npmjs.com/package/cors) - Libreria de express para permitir las conexiones a la api
* [Material-UI](https://material-ui.com/) - Compenentes de estilos para React con la guia de diseño de Google
* [Notistack](https://iamhosseindhv.com/notistack/demos) - Notificaciones para el usuario
* [React-google-charts](https://react-google-charts.com/) - Mostar graficos estadisticos

## Autores ✒️

_Los autores del proyecto_

* **Toro Vega Ider** 
* **Morales Sanchez Luis** 
* **Velez Carbo Rodrigo** 
* **Marcillo Villafuerte Solange** 
