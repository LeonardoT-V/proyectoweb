const express = require('express')
const cors = require('cors')
const app = express();

//middlewares
app.use(express.json())
app.use(cors())

//Directorio publico
app.use(express.static('public'))

//routes
app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/registro', require('./routes/registroUsuario.routes'))
app.use('/api/crear-encuesta', require('./routes/crearEncuesta.routes'))
app.use('/api/pregunta', require('./routes/pregunta.routes'))
app.use('/api/encuesta', require('./routes/encuestas.routes'))
app.use('/api/responder', require('./routes/responderEncuesta.routes'))
app.use('/api/contactanos', require('./routes/contactanos.routes'))

const PORT = process.env.PORT || 4000;

app.listen(PORT);
console.log("escuchando puerto " + PORT);