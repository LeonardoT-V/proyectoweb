const { Router } = require('express')
const routes = Router()
const { consultarPregunta, eliminarPregunta, agregarPregunta, mostrarPreguntas, modificarPregunta } = require('../controllers/pregunta.controller')


routes.post('/', agregarPregunta);
routes.get('/:id', mostrarPreguntas);
routes.get('/p/:id', consultarPregunta);
routes.delete('/:id', eliminarPregunta);
routes.put('/:id', modificarPregunta);

module.exports = routes