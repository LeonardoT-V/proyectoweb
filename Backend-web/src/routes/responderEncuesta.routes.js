const { Router } = require('express')
const routes = Router()
const { responderPreguntas, verResultadosPreguntas, verPreguntaConTotalRespondido } = require('../controllers/responderEncuesta.controller')

routes.post('/:id', responderPreguntas)
routes.get('/:id', verResultadosPreguntas)
routes.get('/r/:id', verPreguntaConTotalRespondido)

module.exports = routes