const { Router } = require('express')
const routes = Router()
const { ingresarEncuesta, publicarEncuesta, eliminarEncuesta, modificarEncuesta, conseguirEncuestaActual } = require('../controllers/crearEncuesta.controller')

routes.post('/', ingresarEncuesta);
routes.put('/:id', modificarEncuesta);
routes.delete('/:id', eliminarEncuesta);
routes.put('/pub/:id', publicarEncuesta)
routes.get('/:id', conseguirEncuestaActual)
module.exports = routes