const { Router } = require('express')
const routes = Router()
const { postUser } = require('../controllers/registroUsuario.controller')

routes.post('/', postUser);

module.exports = routes
