const { Router } = require('express')
const routes = Router()
const { formularioContactanos } = require('../controllers/contactanos.controller')

routes.post('/', formularioContactanos)

module.exports = routes