const { Pool } = require('pg')

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: 'q1234',
    database: 'uleamFORM',
    port: '5432'
})

module.exports = pool;