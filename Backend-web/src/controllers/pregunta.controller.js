const pool = require('../db/coneccionDB');

const agregarPregunta = async (req, res) => {
    console.log(req.body);
    const { id_encuesta, titulo_pregunta, opcion_1, opcion_2, opcion_3, opcion_4 } = req.body;
    const response = await pool.query(`INSERT INTO Pregunta
    (id_encuesta_pregunta,titulo_pregunta,opcion_1,opcion_2,opcion_3,opcion_4)
    values ($1,$2,$3,$4,$5,$6)`
        , [id_encuesta, titulo_pregunta, opcion_1, opcion_2, opcion_3, opcion_4]);
    console.log(response.rows);
    res.status(200).json({ msg: 'pregunta agregada' })
}

const mostrarPreguntas = async (req, res) => {
    const id_encuesta = req.params.id;
    const response = await pool.query(`select * from Pregunta where id_encuesta_pregunta=$1 ORDER BY id_pregunta`, [id_encuesta]);
    res.status(200).json({ recibirDatos: response.rows })
}
const consultarPregunta = async (req, res) => {
    const id_pregunta = req.params.id;
    const response = await pool.query(`select * from Pregunta where id_pregunta=$1`, [id_pregunta]);
    res.status(200).json({ datos: response.rows[0] })
}

const eliminarPregunta = async (req, res) => {
    const id_pregunta = req.params.id;
    const response = await pool.query('DELETE from Pregunta where id_pregunta=$1', [id_pregunta])
    res.status(200).json({ msg: 'pregunta eliminada correctamente' })
}

const modificarPregunta = async (req, res) => {
    const { titulo_pregunta, opcion_1, opcion_2, opcion_3, opcion_4, id_encuesta } = req.body;
    const id_pregunta = req.params.id;
    const response = await pool.query(`UPDATE Pregunta SET
    titulo_pregunta=$1,opcion_1=$2,opcion_2=$3,opcion_3=$4,opcion_4=$5,id_encuesta_pregunta=$6
    WHERE id_pregunta=$7`
        , [titulo_pregunta, opcion_1, opcion_2, opcion_3, opcion_4, id_encuesta, id_pregunta]);

    res.status(200).json({
        msg: 'pregunta modificada',
    })
}


module.exports = {
    agregarPregunta,
    mostrarPreguntas,
    eliminarPregunta,
    modificarPregunta,
    consultarPregunta
}