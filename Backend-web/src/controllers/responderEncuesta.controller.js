const pool = require('../db/coneccionDB');

const responderPreguntas = async (req, res) => {

    const body = req.body;
    console.log(body);
    for (id in body) {
        const response = await pool.query(`INSERT INTO Respuesta
            (id_pregunta_respuesta,value_respuesta)
            VALUES ($1, $2)`, [id, body[id]])
    }
}

const verResultadosPreguntas = async (req, res) => {
    const id = req.params.id;
    const data = [];
    const response = await pool.query(`SELECT 
    count(value_respuesta),value_respuesta
        FROM Respuesta
        WHERE id_pregunta_respuesta = $1
        GROUP BY value_respuesta`, [id]);

    response.rows.map((datos, index) => {
        if (index === 0) {
            data.push(['pregunta', 'valor'])
        }
        data.push([datos.value_respuesta, parseInt(datos.count)])
    })
    res.status(200).send(data)
}
const verPreguntaConTotalRespondido = async (req, res) => {
    const id_encuesta = req.params.id;
    const response = await pool.query(`
    select 
    count(id_pregunta_respuesta) AS total_pregunta ,
    pregunta.titulo_pregunta AS titulo_pregunta,
	pregunta.id_pregunta AS id_pregunta
    FROM respuesta
        INNER JOIN pregunta ON
        respuesta.id_pregunta_respuesta = pregunta.id_pregunta
    WHERE pregunta.id_encuesta_pregunta= $1
    GROUP BY titulo_pregunta , id_pregunta
	ORDER BY id_pregunta;`, [id_encuesta]);
    res.status(200).json({ recibirDatos: response.rows })
}
module.exports = {
    responderPreguntas,
    verResultadosPreguntas,
    verPreguntaConTotalRespondido
}