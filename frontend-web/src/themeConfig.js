import { createTheme } from '@material-ui/core/styles'

const theme = createTheme({
    palette: {
        primary: {
            main: '#689D6A'
        },
        secondary: {
            main: '#F44336'
        }
    },

})

export default theme;