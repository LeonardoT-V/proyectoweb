import React from 'react'
import './nosotros.css'
import Navbar from '../navbar/Navbar'

import foto_universidad_3 from '../../img/foto-universidad-3.jpg'
import foto_universidad_4 from '../../img/foto-universidad-4.jpg'
import foto_universidad_1 from '../../img/foto-universida-1.jpg'
import foto_universidad_2 from '../../img/foto-universidad-2.jpg'
import bandera from '../../img/bandera.jpeg'
import escudo from '../../img/escudo.jpg'
export default function Nosotros() {
    return (
        <>
            <Navbar />
            <section className="contenedor">
                <h2 className="titulo">quienes somos ?</h2>
                <div className="contenedor-somos">
                    <div className="somos">
                        <img src={foto_universidad_3} alt="foto de la universidad 3" />
                        <p>La Universidad Laica “Eloy Alfaro” de Manabí, creada mediante Ley No. 10 publicada en el Registro
                            Oficial No. 313 de noviembre 13 de 1985, es una institución de Educación Superior, con personería
                            jurídica de derecho público sin fines de lucro, de carácter laico, autónoma, democrática,
                            pluralista, crítica y científica.La Universidad Laica “Eloy Alfaro” de Manabí tiene su sede en
                            Manta, una de las cinco principales ciudades del Ecuador, ciudad ribereña al mar, centro pesquero de
                            los mas importantes del Pacifico Sur y ciudad de gran potencialidad en cuanto a desarrollo
                            turístico, es además una ciudad que se proyecta a futuro como posible puerto de transferencia
                            internacional. La Universidad fundamentalmente sirve a la juventud de la tercera provincia del
                            Ecuador que tiene una población que supera el millón doscientos mil habitantes.
                        </p>
                    </div>
                    <div className="somos">
                        <p>La Universidad entrega a Manta un incuantificable aporte para que esta ciudad se convierta en una
                            ciudad de pujante desarrollo. Es una Universidad de carácter humanista, con una clara concepción
                            laica en materia educativa que procura la más exigente libertad de enseñanza y cátedra, entendiendo
                            al estudiante como el gran actor de su proceso de formación y al docente como el gran facilitador
                            del futuro profesional. En este contexto concibe su oferta académica con la mas amplia diversidad, a
                            efectos de responder a las diferentes aspiraciones de los jóvenes que desean seguir una carrera
                            universitaria, entendiendo bien que los procesos educativos son procesos dinámicos por lo que
                            anualmente reajusta su oferta educativa adecuándola a los requerimientos de la juventud y a la
                            acelerada evolución del mundo contemporáneo.</p>
                        <img src={foto_universidad_4} alt="foto universidad 4" />
                    </div>
                    <div>
                        <p>La Universidad ha privilegiado un trabajo académico sistemático en la capacitación y actualización de
                            conocimientos del personal docente y cuenta para su organización que es una de sus fortalezas con
                            una normativa jurídica y reglamentaria muy consistente. En el fondo existe un trabajo cordinado de
                            autoridades, unidades académicas y administrativas, debidamente articulados y cohesionados en
                            función de objetivos institucionales, pues a criterio del rector un buen directivo es un buen equipo
                            de trabajo.</p>
                    </div>
                    <button className="btn">Visita nuestra pagina</button>
                </div>
            </section>

            <section className="contenedor">
                <div className="vision-mision">
                    <div className="misvis">
                        <div className="altura-misvis">
                            <h3 className="sub-titulo titulo-mision">Misión</h3>
                            <p>Formar profesionales competentes y emprendedores desde lo académico, la investigación, y la
                                vinculación, que contribuyan a mejorar la calidad de vida de la sociedad.</p>
                        </div>
                        <hr className="divisor-misvis" />
                        <img src={foto_universidad_1} alt="foto de universidad" className="img-mision" />
                    </div>
                    <div className="misvis">
                        <img src={foto_universidad_2} alt="foto de universidad" className="img-vision" />
                        <hr className="divisor-misvis" />
                        <div className="altura-misvis">
                            <h3 className="sub-titulo titulo-vision">Visión</h3>
                            <p>Ser un referente nacional e internacional de Institución de Educación Superior que contribuye al
                                desarrollo social, cultural y productivo con profesionales éticos, creativos, cualificados y con
                                sentido de pertinencia.</p>
                        </div>
                    </div>
                </div>

            </section>
            <section className="contenedor">
                <h2 className="titulo">simbolos universitarios</h2>
                <div className="simbolos">
                    <div className="escudo">
                        <h3 className="sub-titulo">Escudo</h3>
                        <img src={escudo} alt="escudo ULEAM" className="img-simbolos" />
                    </div>
                    <div className="himno">
                        <h3 className="sub-titulo">Himno</h3>
                        <div className="letra-himno">
                            <p className="text-fuerte">Coro</p>
                            <p>Luchador de la espada y la idea<br />
                                Gran maestro de la libertad<br />
                                es Alfaro y es nuestra tarea<br />
                                hacer patria en su nombre inmortal<br /></p>
                        </div>
                        <div className="letra-himno">
                            <p className="text-fuerte">Estrofa</p>
                            <p>Alma Mater en ti se agiganta<br />
                                nuestro anhelo de un nuevo Ecuador<br />
                                un bastión del derecho habrá en Manta<br />
                                y una altiva tribuna de honor Nuestra Patria es América inmensa<br />
                                su destino nos hace vivir<br />
                                por su causa se lucha y se piensa<br />
                                con Bolívar, Alfaro y Martí</p>
                        </div>
                    </div>
                    <div className="bandera">
                        <h3 className="sub-titulo">Bandera</h3>
                        <img src={bandera} alt="bandera ULEAM" className="img-simbolos" />
                    </div>
                </div>
            </section>


            <footer className="footer">
                <p>ULEAM © Copyright 2020, Todos los derechos reservados - Universidad Laica Eloy Alfaro de Manabí </p>
                <p>Dirección: Av. Circunvalación - Vía a San Mateo </p>
                <p>Manta - Manabí - Ecuador </p>
            </footer>

        </>
    )
}
