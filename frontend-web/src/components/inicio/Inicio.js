import React from 'react'
import './inicio.css'
import { Link } from 'react-router-dom' /* modulo para la creacion de rutas */
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Navbar from '../navbar/Navbar'
import Clima from '../apiClima/Clima'
import '../../app.css'
import theme from '../../themeConfig'
const useStyle = makeStyles({
    enlaces: {
        color: 'white',
        textTransform: 'none',
        textDecoration: 'none',
        padding: '10px 0px'
    }
})
export default function Inicio() {
    const estilos = useStyle()

    const datosUsuarios = JSON.parse(localStorage.getItem('usuario'));

    return (
        <ThemeProvider theme={theme}>
            <Navbar />

            <header className="cabecero">
                <div className="cabecero-titulo">
                    <h2>ULEAM Forms</h2>
                    <p>Crear tus encuestas para tus estudios o proyectos</p>
                </div>
                <div className="cabecero-empieza">
                    <h2>Empieza ahora</h2>
                    <Button variant="contained" size="large" color="primary" type="submit">
                        {!datosUsuarios
                            ? <Link to="/login" className={estilos.enlaces}>Crear Encuesta</Link>
                            : <Link to="/crear-encuesta" className={estilos.enlaces}>Crear Encuesta</Link>
                        }
                    </Button>
                </div>
            </header>
            <main className="contenedor-secciones main-div">
                <div>
                    <h1 className="secciones-titulos">Crea encuestas de acuerdo a tus necesidades </h1>
                    <ul className="main-lista">
                        <li>Software diseñado para estudiantes y docentes</li>
                        <li>Crear encustas y compartelas con todos</li>
                        <li>Facil de usar</li>
                    </ul>
                </div>
                <div>
                    <h2 className="secciones-titulos">Compatible con todos los dispositivos</h2>
                    <div className="dispositivos-contenedor">
                        <div className="dispositivos-flex">
                            <i className="fas fa-mobile-alt"></i>
                            <p>Moviles</p>
                        </div>
                        <div className="dispositivos-flex">
                            <i className="fas fa-laptop"></i>
                            <p>Laptops</p>
                        </div>
                        <div className="dispositivos-flex">
                            <i className="fas fa-desktop"></i>
                            <p>Escritorios</p>
                        </div>
                    </div>
                </div>
            </main>
            <hr className="line-divisor" />
            <section className="contenedor-secciones">
                <h2 className="secciones-titulos">Crea y Responde Donde Estes</h2>
                <p className="centrar-text-secciones">Puedes crear o responder formulario fuera de tu hogar con una <br /> conexion a
                    internet</p>
                <div className="centrar-text-secciones secciones-i"><i className="fas fa-wifi"></i></div>
            </section>
            <hr className="line-divisor" />
            <section className="contenedor-secciones seccion-boton">
                <h2 className="secciones-titulos">Empieza Ahora</h2>
                <p className="centrar-text-secciones">Comienza a realizar encuestas y compartelos con tus amigos o con la comunidad
                </p>
                <Link to="/crear-encuesta" className="btn">Comienza a crear</Link>
            </section>
            <hr className="line-divisor" />
            <section className="contenedor-secciones seccion-boton">
                <Clima />
            </section>
            <footer className="footer">
                <p>ULEAM © Copyright 2020, Todos los derechos reservados - Universidad Laica Eloy Alfaro de Manabí </p>
                <p>Dirección: Av. Circunvalación - Vía a San Mateo </p>
                <p>Manta - Manabí - Ecuador </p>
            </footer>
        </ThemeProvider>
    )
}
